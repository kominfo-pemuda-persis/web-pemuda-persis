<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        try {
            setlocale(LC_ALL, 'IND');
            DB::statement("SET lc_time_names = 'id_ID'");
            Paginator::useBootstrapFive();
            Carbon::setLocale('id');
            \URL::forceScheme('https');
            // if (env('APP_ENV') === 'production' || env('APP_ENV') === 'dev_https' ) {
            // }
        } catch (\Throwable $th) {
            // prevent error gitlab CI
        }
    }
}
