<?php

namespace App\Models\Home;

use Haruncpi\LaravelUserActivity\Traits\Loggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sejarah extends Model
{
    use HasFactory, Loggable;

    protected $fillable = [
        'nama',
        'keterangan',
        'tahun',
    ];
    protected $primaryKey = 'id';
    protected $table = 'home_sejarah';
    const tableName = 'home_sejarah';
}
