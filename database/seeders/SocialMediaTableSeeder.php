<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SocialMediaTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('social_media')->delete();
        
        \DB::table('social_media')->insert(array (
            0 => 
            array (
                'id' => 5,
                'nama' => 'Instagram',
                'icon' => 'fab fa-instagram',
                'url' => 'https://www.instagram.com/pp_pemudapersis/',
                'order' => 1,
                'keterangan' => 'PP Pemuda Persis Official',
                'status' => 1,
                'created_at' => '2023-11-20 21:14:13',
                'updated_at' => '2023-11-20 21:14:13',
            ),
            1 => 
            array (
                'id' => 6,
                'nama' => 'Facebook',
                'icon' => 'fab fa-facebook-square',
                'url' => 'https://web.facebook.com/kominfopemudapersis',
                'order' => 2,
                'keterangan' => 'Kominfo Pemuda Persis',
                'status' => 1,
                'created_at' => '2023-11-20 21:15:13',
                'updated_at' => '2023-11-20 21:15:13',
            ),
            2 => 
            array (
                'id' => 7,
                'nama' => 'Twitter',
                'icon' => 'fab fa-twitter-square',
                'url' => 'https://twitter.com/pp_pemudapersis',
                'order' => 3,
                'keterangan' => 'PP Pemuda Persis',
                'status' => 1,
                'created_at' => '2023-11-20 21:16:05',
                'updated_at' => '2023-11-20 21:16:05',
            ),
            3 => 
            array (
                'id' => 8,
                'nama' => 'Youtube',
                'icon' => 'fab fa-youtube',
                'url' => 'https://www.youtube.com/c/pppemudapersis',
                'order' => 4,
                'keterangan' => 'PP Pemuda Persis',
                'status' => 1,
                'created_at' => '2023-11-20 21:17:38',
                'updated_at' => '2023-11-20 21:17:38',
            ),
            4 => 
            array (
                'id' => 9,
                'nama' => 'Tik Tok',
                'icon' => 'fab fa-tiktok',
                'url' => 'https://www.tiktok.com/@pp_pemudapersis',
                'order' => 5,
                'keterangan' => 'Pemuda Persis',
                'status' => 1,
                'created_at' => '2023-11-20 21:18:27',
                'updated_at' => '2023-11-20 21:18:27',
            ),
            5 => 
            array (
                'id' => 10,
                'nama' => 'Telegram',
                'icon' => 'fab fa-telegram',
                'url' => 'https://t.me/pemudapersis',
                'order' => 6,
                'keterangan' => 'Channel Telegram Pemuda Persis',
                'status' => 1,
                'created_at' => '2023-11-20 21:20:47',
                'updated_at' => '2023-11-20 21:20:47',
            ),
            6 => 
            array (
                'id' => 11,
                'nama' => 'Linkedin',
                'icon' => 'fab fa-linkedin',
                'url' => 'https://www.linkedin.com/in/pp-pemudapersis/',
                'order' => 7,
                'keterangan' => 'PP Pemuda Persis',
                'status' => 1,
                'created_at' => '2023-11-20 21:23:56',
                'updated_at' => '2023-11-20 21:23:56',
            ),
            7 => 
            array (
                'id' => 12,
                'nama' => 'Website',
                'icon' => 'fas fa-globe-asia',
                'url' => 'https://anaonline.id/',
                'order' => 8,
                'keterangan' => 'Website Utama',
                'status' => 1,
                'created_at' => '2023-11-20 21:25:03',
                'updated_at' => '2023-11-20 21:25:03',
            ),
        ));
        
        
    }
}