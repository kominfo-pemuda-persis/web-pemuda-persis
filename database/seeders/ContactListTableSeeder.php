<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ContactListTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('contact_list')->delete();
        
        \DB::table('contact_list')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nama' => 'Location',
                'icon' => 'fas fa-map-marker-alt',
                'url' => 'https://maps.app.goo.gl/pFCtZ2JogNb1jST86',
                'order' => 1,
                'keterangan' => 'Jl. Perintis Kemerdekaan No.2, Babakan Ciamis, Kec. Sumur Bandung, Kota Bandung',
                'status' => 1,
                'created_at' => '2022-08-21 08:34:56',
                'updated_at' => '2023-11-20 18:28:05',
            ),
            1 => 
            array (
                'id' => 2,
                'nama' => 'Telegram',
                'icon' => 'fab fa-telegram',
                'url' => 'https://t.me/pemudapersis',
                'order' => 2,
                'keterangan' => 'pemudapersis',
                'status' => 1,
                'created_at' => '2022-08-21 08:35:23',
                'updated_at' => '2023-11-20 18:28:53',
            ),
            2 => 
            array (
                'id' => 3,
                'nama' => 'Instagram',
                'icon' => 'fab fa-instagram',
                'url' => 'https://www.instagram.com/pp_pemudapersis/',
                'order' => 3,
                'keterangan' => '@pp_pemudapersis',
                'status' => 1,
                'created_at' => '2022-08-21 08:35:47',
                'updated_at' => '2023-11-20 21:12:29',
            ),
        ));
        
        
    }
}